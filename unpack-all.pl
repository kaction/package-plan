#!/usr/bin/perl

# Unpacks all packages to ./unpacked with patches applied

use strict;
use warnings;

use Dpkg::Version;
use Cwd;
use File::Copy;
use Archive::Tar;
use File::Slurp;
use IPC::Run qw( run );
use Getopt::Long;

my %packages;
my %versions;
my %available;
my %comments;
my %notest;
my @flags;

my $allow_upgrade = 0;

GetOptions (
	) or die("Error in command line arguments\n");

print "Reading packages.txt...\n";
open PACKAGES, "<", "packages.txt" or die $!;
while (<PACKAGES>) {
	chomp;
	next if /^#/;
	next if /^\s*$/;
	unless (m/^(.*?) (.*?)(?: (.*))?$/) {
		print "Ignoring unparseable line $.: $_\n";
	}
	my ($pkg,$version,$comment) = ($1,$2,$3);
	$packages{$pkg}++;
	$versions{$pkg} = $version;
	next unless $comment;
	$notest{$pkg}++ if $comment =~ s/notest\s*//;
	push @flags, $1 while $comment =~ s/(-f[^ ]+)\s*//;
	$comments{$pkg} = $comment if $comment;
}
close PACKAGES;

my $destdir = cwd() . "/unpacked";
system("rm","-rf",$destdir);
mkdir "$destdir" or die $!;

print "Unpacking all packages\n";
for my $pkg (sort keys %versions) {
	next if (exists $comments{$pkg});
	my $pkgid = sprintf "%s-%s", $pkg, $versions{$pkg};
	print "Unpacking $pkgid...";
	my $out;
	my $stderr;
	if (run ["cabal", "unpack", "-d", $destdir, $pkgid], \"", \$out, \$stderr) {
		print "done\n";
	} else {
		print "failed!\n";
		print $stderr;
	}
	my $patchesdir = sprintf "./patches/%s/%s", $pkg, $versions{$pkg};
	if (-d "$patchesdir") {
		print "Patching $pkgid...";
		symlink "../../$patchesdir", "$destdir/$pkgid/patches" or die @!;
		chdir "$destdir/$pkgid" or die @!;
		if (run ["quilt", "push", "-a"], \"", \$out, \$stderr) {
			print "done\n";
		} else {
			print "failed!\n";
			print $stderr;
		}
		chdir "../.." or die @!;
	}
}
